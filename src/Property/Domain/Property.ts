import { PropertyName } from "./ValueObject/PropertyName";
import { randomUUID } from "crypto";

export class Property {
    public id: string;

    constructor(public name: PropertyName) {
        this.id = randomUUID()
    }
}
