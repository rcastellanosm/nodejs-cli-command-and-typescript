import { InvalidPropertyNameException } from "../Excepetion/InvalidPropertyNameException";

export class PropertyName {
    constructor(name: string) {
        if(name === undefined) {
            throw new InvalidPropertyNameException(name);
        }
    }
}
