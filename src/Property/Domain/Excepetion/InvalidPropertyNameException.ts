export class InvalidPropertyNameException extends Error {
    constructor(message: string) {

        super(`Invalid property name provided => ${message}`);

        this.name = this.constructor.name;

        Error.captureStackTrace(this, this.constructor);
    }

}
