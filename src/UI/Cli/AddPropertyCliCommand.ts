import type { Arguments, CommandBuilder } from 'yargs';
import { Property } from "../../Property/Domain/Property";
import { PropertyName } from "../../Property/Domain/ValueObject/PropertyName";

type Options = {
    name: string;
};

export const command: string = 'property:add <name>';
export const desc: string = 'Add a new property to storage';

export const builder: CommandBuilder<Options, Options> = (yargs) =>
    yargs
        .positional('name', { type: 'string', demandOption: true });

export const handler = (argv: Arguments<Options>): void => {
    const { name } = argv;
    console.log((new Property(new PropertyName(name))).id)
    process.exit(0);
};


