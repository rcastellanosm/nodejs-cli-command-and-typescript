# NodeJS with TypeScript (Implementing DDD + CQRS)
A simple NodeJS application using Typescript and implementing DDD + CQRS approach with bus dispatch. I use a memory repository persistence for demo purposes 

## Requirements
````bash
NodeJS 17.0+
Yarn 1.22+
Docker (Engine + Compose) 1.19+
````

## Usage
This example come with 2 commands to describe the implementation of CQRS + DDD on a NodeJS application using TypeScript

### Without Docker
````bash
$ yarn install
$ yarn build
$ bin/console help
$ bin/console property:add <name> <price>
````

### Dockerized
A Dockerfile and docker-compose file are supplied

````bash
$ docker-compose up build -d
$ docker -it cli /bin/bash -c "bin/console help"
$ docker -it cli /bin/bash -c "property:add <name> <price>"
````

## Testing
If you want to run the test suites
````bash
$ yarn test
$ yarn coverage 
`````
